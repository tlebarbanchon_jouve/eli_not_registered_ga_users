/* 
 * ContentAlert.Service V1.0
 *
 * The content alert service.
 *
 * OpenAPI spec version: 1.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */
using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;
using SwaggerDateConverter = IO.Swagger.Client.SwaggerDateConverter;

namespace IO.Swagger.Model
{
    /// <summary>
    /// AlertProcessingReport
    /// </summary>
    [DataContract]
        public partial class AlertProcessingReport :  IEquatable<AlertProcessingReport>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AlertProcessingReport" /> class.
        /// </summary>
        /// <param name="count">Total number of alert processed.</param>
        /// <param name="success">Number of alert processed with success.</param>
        /// <param name="failed">Number of alert failed.</param>
        /// <param name="errorMessages">List of error encountered during the processing of the alert.</param>
        /// <param name="warningMessages">List of error encountered during the processing that are not blocker.</param>
        /// <param name="userEmails">List of the e-mails address processed with success.</param>
        /// <param name="startDate">Datetime when the process started..</param>
        /// <param name="endDate">Datetime when the process ended..</param>
        public AlertProcessingReport(int? count = default(int?), int? success = default(int?), int? failed = default(int?), List<string> errorMessages = default(List<string>), List<string> warningMessages = default(List<string>), string userEmails = default(string), DateTime? startDate = default(DateTime?), DateTime? endDate = default(DateTime?))
        {
            this.Count = count;
            this.Success = success;
            this.Failed = failed;
            this.ErrorMessages = errorMessages;
            this.WarningMessages = warningMessages;
            this.UserEmails = userEmails;
            this.StartDate = startDate;
            this.EndDate = endDate;
        }
        
        /// <summary>
        /// Total number of alert processed
        /// </summary>
        /// <value>Total number of alert processed</value>
        [DataMember(Name="count", EmitDefaultValue=false)]
        public int? Count { get; set; }

        /// <summary>
        /// Number of alert processed with success
        /// </summary>
        /// <value>Number of alert processed with success</value>
        [DataMember(Name="success", EmitDefaultValue=false)]
        public int? Success { get; set; }

        /// <summary>
        /// Number of alert failed
        /// </summary>
        /// <value>Number of alert failed</value>
        [DataMember(Name="failed", EmitDefaultValue=false)]
        public int? Failed { get; set; }

        /// <summary>
        /// List of error encountered during the processing of the alert
        /// </summary>
        /// <value>List of error encountered during the processing of the alert</value>
        [DataMember(Name="error-messages", EmitDefaultValue=false)]
        public List<string> ErrorMessages { get; set; }

        /// <summary>
        /// List of error encountered during the processing that are not blocker
        /// </summary>
        /// <value>List of error encountered during the processing that are not blocker</value>
        [DataMember(Name="warning-messages", EmitDefaultValue=false)]
        public List<string> WarningMessages { get; set; }

        /// <summary>
        /// List of the e-mails address processed with success
        /// </summary>
        /// <value>List of the e-mails address processed with success</value>
        [DataMember(Name="user-emails", EmitDefaultValue=false)]
        public string UserEmails { get; set; }

        /// <summary>
        /// Datetime when the process started.
        /// </summary>
        /// <value>Datetime when the process started.</value>
        [DataMember(Name="start-date", EmitDefaultValue=false)]
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Datetime when the process ended.
        /// </summary>
        /// <value>Datetime when the process ended.</value>
        [DataMember(Name="end-date", EmitDefaultValue=false)]
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class AlertProcessingReport {\n");
            sb.Append("  Count: ").Append(Count).Append("\n");
            sb.Append("  Success: ").Append(Success).Append("\n");
            sb.Append("  Failed: ").Append(Failed).Append("\n");
            sb.Append("  ErrorMessages: ").Append(ErrorMessages).Append("\n");
            sb.Append("  WarningMessages: ").Append(WarningMessages).Append("\n");
            sb.Append("  UserEmails: ").Append(UserEmails).Append("\n");
            sb.Append("  StartDate: ").Append(StartDate).Append("\n");
            sb.Append("  EndDate: ").Append(EndDate).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as AlertProcessingReport);
        }

        /// <summary>
        /// Returns true if AlertProcessingReport instances are equal
        /// </summary>
        /// <param name="input">Instance of AlertProcessingReport to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(AlertProcessingReport input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.Count == input.Count ||
                    (this.Count != null &&
                    this.Count.Equals(input.Count))
                ) && 
                (
                    this.Success == input.Success ||
                    (this.Success != null &&
                    this.Success.Equals(input.Success))
                ) && 
                (
                    this.Failed == input.Failed ||
                    (this.Failed != null &&
                    this.Failed.Equals(input.Failed))
                ) && 
                (
                    this.ErrorMessages == input.ErrorMessages ||
                    this.ErrorMessages != null &&
                    input.ErrorMessages != null &&
                    this.ErrorMessages.SequenceEqual(input.ErrorMessages)
                ) && 
                (
                    this.WarningMessages == input.WarningMessages ||
                    this.WarningMessages != null &&
                    input.WarningMessages != null &&
                    this.WarningMessages.SequenceEqual(input.WarningMessages)
                ) && 
                (
                    this.UserEmails == input.UserEmails ||
                    (this.UserEmails != null &&
                    this.UserEmails.Equals(input.UserEmails))
                ) && 
                (
                    this.StartDate == input.StartDate ||
                    (this.StartDate != null &&
                    this.StartDate.Equals(input.StartDate))
                ) && 
                (
                    this.EndDate == input.EndDate ||
                    (this.EndDate != null &&
                    this.EndDate.Equals(input.EndDate))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.Count != null)
                    hashCode = hashCode * 59 + this.Count.GetHashCode();
                if (this.Success != null)
                    hashCode = hashCode * 59 + this.Success.GetHashCode();
                if (this.Failed != null)
                    hashCode = hashCode * 59 + this.Failed.GetHashCode();
                if (this.ErrorMessages != null)
                    hashCode = hashCode * 59 + this.ErrorMessages.GetHashCode();
                if (this.WarningMessages != null)
                    hashCode = hashCode * 59 + this.WarningMessages.GetHashCode();
                if (this.UserEmails != null)
                    hashCode = hashCode * 59 + this.UserEmails.GetHashCode();
                if (this.StartDate != null)
                    hashCode = hashCode * 59 + this.StartDate.GetHashCode();
                if (this.EndDate != null)
                    hashCode = hashCode * 59 + this.EndDate.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }
}
