﻿using OfficeOpenXml;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;

namespace ELI_NOT_REGISTERED_GA_USERS.Services
{
    public static class ExcelService
    {
        public static List<string> ReadExcelFile(string file)
        {
            List<string> emails = new List<string>();

            FileInfo fi = new FileInfo(file);

            using (ExcelPackage excel = new ExcelPackage(fi))
            {
                //Get a WorkSheet by index. Note that EPPlus indexes are base 1, not base 0!
                ExcelWorksheet firstWorksheet = excel.Workbook.Worksheets[1];
                emails = firstWorksheet.Cells.Where(c => !string.IsNullOrEmpty(c.Text) && c.Text.Contains("@")).Select(c => c.Text).ToList();
            }

            return emails;
        }
    }
}
