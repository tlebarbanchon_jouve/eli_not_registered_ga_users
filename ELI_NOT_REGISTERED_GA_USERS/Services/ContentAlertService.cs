﻿using IO.Swagger.Api;
using System.Collections.Generic;
using System;
using System.Linq;
using IO.Swagger.Model;

namespace ELI_NOT_REGISTERED_GA_USERS.Services
{
    public class ContentAlertService
    {
        public static void CreateConfigurations(string uri, List<string> emails, LogService log)
        {
            log.WriteLine("ContentAlertService - Début du traitement : ");

            ContentAlertApi ca = new ContentAlertApi(uri);

            foreach (var email in emails)
            {
                log.WriteLine($"\t Traitement email : {email}");

                var existingConfigs = ca.ContentAlertGetAlertListByUserId(emailAddress: email, clientId: "ElNewsEli");

                if (existingConfigs == null || existingConfigs.Count == 0)
                    log.WriteLine($"\t Aucune configuration existante");
                else
                {
                    //On vire les confs ?
                    log.WriteLine($"\t Configurations trouvées ! Suppression...");
                    ca.ContentAlertDeleteAlerts(email);
                }


                log.WriteLine($"\t Création d'une nouvelle configuration");

                
                var retDaily = ca.ContentAlertPostAlert(new IO.Swagger.Model.UserAlertResource
                {
                    ClientId = "ElNewsEli",
                    CreationDate = DateTime.Now,
                    DataProviderPostPayload = "",
                    DataProviderUrl = "https://www.lemediasocial.fr/endpoint/daily-news-letter",
                    EmailAddress = email,
                    Frequency = 1,
                    IsActive = true,
                    LastExecutionDate = DateTime.Now,
                    ModificationDate = DateTime.Now,
                    TemplateId = "ELNEWSELI_DAILY",
                    UserId = email
                });
                log.WriteLine($"\t\t Daily : {retDaily}");

                var retWeekly = ca.ContentAlertPostAlert(new IO.Swagger.Model.UserAlertResource
                {
                    ClientId = "ElNewsEli",
                    CreationDate = DateTime.Now,
                    DataProviderPostPayload = "",
                    DataProviderUrl = "https://www.lemediasocial.fr/endpoint/weekly-news-letter",
                    EmailAddress = email,
                    Frequency = 7,
                    IsActive = true,
                    LastExecutionDate = DateTime.Now,
                    ModificationDate = DateTime.Now,
                    TemplateId = "ELNEWSELI_WEEKLY",
                    UserId = email
                });
                log.WriteLine($"\t\t Weekly : {retWeekly}");
            }


            

            log.WriteLine("ContentAlertService - Fin du traitement");
        }

        public static void PatchBadDataProviderPostPayloadConfigurations(string contentAlertServiceUri, List<UserAlertResource> confs, LogService log)
        {
            log.WriteLine("ContentAlertService - Début du traitement : ");
            
            foreach (var conf in confs)
            {               
                ContentAlertApi ca = new ContentAlertApi(contentAlertServiceUri);
                log.WriteLine($"\t Traitement configuration : {conf.EmailAddress} - {conf.Id}");

                ca.ContentAlertDeleteServiceInstance(conf.Id);
                conf.DataProviderPostPayload = "";               
                var ret = ca.ContentAlertPostAlert(conf);
                log.WriteLine($"\t\t Nouvel Id : {ret}");
            }

            log.WriteLine("ContentAlertService - Fin du traitement");
        }

        public static List<UserAlertResource> GetBadDataProviderPostPayloadConfigurations(string contentAlertServiceUri)
        {

            ContentAlertApi ca = new ContentAlertApi(contentAlertServiceUri);
            var confs = ca.ContentAlertGetAlertListByUserId(clientId: "ElNewsEli");

            var t = confs.Where(c => c.DataProviderPostPayload != null).ToList();
            var t2 = confs.Where(c => c.DataProviderPostPayload == null).ToList();

            return confs.Where(c => c.DataProviderPostPayload != "").ToList();            
        }
    }
}
