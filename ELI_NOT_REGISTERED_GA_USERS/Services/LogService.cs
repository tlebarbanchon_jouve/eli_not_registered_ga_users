﻿using System;
using System.IO;

namespace ELI_NOT_REGISTERED_GA_USERS.Services
{
    public class LogService : IDisposable
    {
        public string FilePath { get; set; }

        private TextWriter streamWriter { get; set; }


        public LogService(string filePath)
        {
            if (string.IsNullOrEmpty(filePath))
                throw new ArgumentNullException("filePath is mandatory");

            this.streamWriter = new StreamWriter(filePath);
        }

        public void Dispose()
        {
            if (streamWriter != null)
            {
                streamWriter.Flush();
                streamWriter.Dispose();
            }


            GC.SuppressFinalize(this);
        }

        public void WriteLine(string line, bool horodatage = true)
        {
            if (this.streamWriter == null)
                throw new Exception("An error occured");

            if (horodatage)
                line = $"{DateTime.Now.ToString("dd/MM/yyyy - hh:mm:ss")} - {line}";

            Console.WriteLine(line);
            streamWriter.WriteLine(line);
        }
    }
}
