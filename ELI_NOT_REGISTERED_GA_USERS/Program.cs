﻿using ELI_NOT_REGISTERED_GA_USERS.Services;
using System;
using System.Configuration;

namespace ELI_NOT_REGISTERED_GA_USERS
{
    class Program
    {
        static void Main(string[] args)
        {
            
            string contentAlertServiceUri = ConfigurationManager.AppSettings["contentAlertServiceBaseUri"];

            Console.WriteLine("Que veux tu faire mon petit loulou ?");
            Console.WriteLine("1 : Regenerer les configurations à partir d'un fichier excel");
            Console.WriteLine("2 : Reprendre les confs ayant un DataProviderPostPayload différent de \"\"");
            var mode = Console.ReadKey();

            if(mode.Key == ConsoleKey.NumPad1)
            {
                string logFilePath = ConfigurationManager.AppSettings["reportLogExcelLocation"];
                string excelFileLocation = ConfigurationManager.AppSettings["excelFileLocation"];                

                using (var log = new LogService(logFilePath))
                {
                    log.WriteLine("Récupération des emails du fichiers excel");
                    var emails = ExcelService.ReadExcelFile(excelFileLocation);
                    log.WriteLine($"{emails.Count} email(s) trouvé(s)");


                    log.WriteLine("Création des configurations de newsletter");
                    ContentAlertService.CreateConfigurations(contentAlertServiceUri, emails, log);

                    Console.WriteLine("Appuyez sur une touche pour terminer...");
                    Console.ReadKey();
                }
            }
            else if (mode.Key == ConsoleKey.NumPad2)
            {
                string logFilePath = ConfigurationManager.AppSettings["reportLogDataProviderLocation"];
                using (var log = new LogService(logFilePath))
                {
                    log.WriteLine("Récupération des configurations ayant un DataProviderPostPayload différent de \"\"");
                    var confs = ContentAlertService.GetBadDataProviderPostPayloadConfigurations(contentAlertServiceUri);

                    log.WriteLine($"{confs.Count} mauvaises configurations(s) trouvée(s)");


                    log.WriteLine("Correction des configurations");
                    ContentAlertService.PatchBadDataProviderPostPayloadConfigurations(contentAlertServiceUri, confs, log);

                    Console.WriteLine("Appuyez sur une touche pour terminer...");
                    Console.ReadKey();
                }
            }
            else
            {
                Console.WriteLine("Bon bah reviens quand tu sera décidé !");
                Console.ReadKey();
            }
        }
    }
}
